//Tạo một class Companies
class Company {
    //Hàm tạo Constructor
    constructor(paramId, paramCompanyName, paramContact, paramCountry){
        this.id = paramId;
        this.company = paramCompanyName;
        this.contact = paramContact;
        this.country = paramCountry;
    }
    //Hàm kiểm tra tên công ty có chứa ID được truyền vào hay không
    checkCompanyById(paramId) {
        return this.id === paramId
    }
}
let companyListArray = [];

//Khởi tạo các class Company
let companyAlfreds = new Company(1, "Alfreds Futterkiste", "Maria Anders", "Germany");
companyListArray.push(companyAlfreds);

let companyCentro = new Company(2, "Centro comercial Moctezuma", "Francisco Chang", "Mexico");
companyListArray.push(companyCentro);

let companyErnst = new Company(3, "Ernst Handel", "Roland Mendel", "Austria");
companyListArray.push(companyErnst);

let companyIsland = new Company(4, "Island Trading", "Helen Bennett", "UK");
companyListArray.push(companyIsland);

let companyLaughing = new Company(5, "Laughing Bacchus Winecellars", "Yoshi Tannamuri", "Canada");
companyListArray.push(companyLaughing);

let companyMagazzini = new Company(6, "Magazzini Alimentari Riuniti", "Giovanni Rovelli", "Italy");
companyListArray.push(companyMagazzini);

var companyListObject = [
    {
        id: 1,
        company: "Alfreds Futterkiste",
        contact: "Maria Anders",
        country: "Germany"
    },
    {
        id: 2,
        company: "Centro comercial Moctezuma",
        contact: "Francisco Chang",
        country: "Mexico"
    },
    {
        id: 3,
        company: "Ernst Handel",
        contact: "Roland Mendel",
        country: "Austria"
    },
    {
        id: 4,
        company: "Island Trading",
        contact: "Helen Bennett",
        country: "UK"
    },
    {
        id: 5,
        company: "Laughing Bacchus Winecellars",
        contact: "Yoshi Tannamuri",
        countr: "Canada"
    },
    {
        id: 6,
        company: "Magazzini Alimentari Riuniti",
        contact: "Giovanni Rovelli",
        country: "Italy"
    }
]

modules.exports = { companyListArray, companyListObject } 