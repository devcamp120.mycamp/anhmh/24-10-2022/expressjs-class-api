//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

const router = express.Router();

//Import Company Data
const { companyListArray, companyListObject } = require("./data");

router.get("/companies", (request, response) => {
    let company = request.query.company;
    if (company) {
        //Khởi tạo biến companyListResponse là kết quả tìm được 
        let companyListObject = [];

        //Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
        for (let index = 0; index < companyListArray.length; index++) {
            let companyElement = companyListArray[index];

            if (companyElement.checkCompanyById(id)) {
                companyListObject.push(companyElement);
            }
        }
        response.status(200).json({
            company: companyListObject
        })
    } else {
        response.status(200).json({
            company: companyListArray
        })

    }
});

router.post("/companies", (request, response) => {
    response.json({
        message: "POST All Company"
    })
});

router.put("/companies", (request, response) => {
    response.json({
        message: "PUT All Company"
    });
});

router.delete("/companies", (request, response) => {
    response.json({
        message: "DELETE All Company"
    });
});

//Export dữ liệu thành 1 module
module.exports = router;
