//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const express = require("express");
const { companyListArray, companyListObject } = require("./app/routes/companyRouter");

//Import Router
const companiesRouter = require("./app/routes/companyRouter");

//Khởi tạo app express
const app = express();

//Khai báo cổng của project 
const port = 8000;

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/companies-class", (request, response) => {
    let query = request.query
    
    response.status(200).json({
        message: "Company API"
        companyListArray
    })
})
app.get("/companies-object", (request, response) => {
    let query = request.query;

    response.status(200).json({
        message: "Company API"
        companyListObject
    })
})

app.use("/", companiesRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng) np " + port);
})